﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab09
{
    public class Add
    {
        public string text;
        public Age ageRange;
        public Hobby interests;

        public Add(string text, Age ageRange, Hobby interests)
        {
            this.text = text;
            this.ageRange = ageRange;
            this.interests = interests;
        }

        public void Test()
        {
            if (ageRange.HasFlag(Age.Kids))
            {
                Console.WriteLine("Kids friendly add");
            }
            if (ageRange >= Age.Adults)
            {
                Console.WriteLine("NSFW add");
            }
        }
    }
}
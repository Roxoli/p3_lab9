﻿using System;

namespace Lab09
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Podziel z reszta");
            int num = Convert.ToInt32(Console.ReadLine());
            int result, remainder;
            (result, remainder) = num.DivideWithRemainder(5);
            Console.WriteLine($"{num}/5 = {result} reszta: {remainder}");
            Console.WriteLine("liczenie liter");
            Console.WriteLine("Napisz cos");
            string text = Convert.ToString(Console.ReadLine());
            Console.WriteLine("Litera do policzenia");
            char letter = Convert.ToChar(Console.ReadLine());
            Console.WriteLine($"{text.CountLetters(letter)}");


            Add ad = new Add("You've won an Iphone!", Age.Kids | Age.Teens, Hobby.Electronics);
            ad.Test();

            Add ad2 = new Add("You've won an Iphone!", Age.Adults | Age.Elders, Hobby.Electronics);
            ad2.Test();
        }
    }
}

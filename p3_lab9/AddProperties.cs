﻿using System;

namespace Lab09
{
    [Flags]
    public enum Age
    {
        None = 0,
        Kids = 1,
        Teens = 2,
        Adults = 4,        
        Elders = 8
    }

    [Flags]
    public enum Hobby
    {
        Electronics,
        Automotive,
        Gaming,
        Economy
    }
}